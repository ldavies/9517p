# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/AbstractFeatureMatcher.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/AbstractFeatureMatcher.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/BundleAdjuster.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/BundleAdjuster.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/Common.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/Common.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/Distance.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/Distance.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/FindCameraMatrices.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/FindCameraMatrices.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/GPUSURFFeatureMatcher.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/GPUSURFFeatureMatcher.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/MultiCameraDistance.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/MultiCameraDistance.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/MultiCameraPnP.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/MultiCameraPnP.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/OFFeatureMatcher.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/OFFeatureMatcher.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/RichFeatureMatcher.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/RichFeatureMatcher.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/SfMUpdateListener.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/SfMUpdateListener.cpp.o"
  "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/Triangulation.cpp" "/home/kingpalex/Pro/code/Chapter4_StructureFromMotion/build2/CMakeFiles/ExploringSfMLibrary.dir/Triangulation.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "DISABLE_PCAP"
  "DISABLE_PNG"
  "USE_EIGEN"
  "EIGEN_USE_NEW_STDVECTOR"
  "EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET"
  "FLANN_STATIC"
  "qh_QHpointer"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
