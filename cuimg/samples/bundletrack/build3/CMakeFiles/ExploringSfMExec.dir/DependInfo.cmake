# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/kingpalex/Uni/9517P/cuimg/samples/bundletrack/Visualization.cpp" "/home/kingpalex/Uni/9517P/cuimg/samples/bundletrack/build3/CMakeFiles/ExploringSfMExec.dir/Visualization.cpp.o"
  "/home/kingpalex/Uni/9517P/cuimg/samples/bundletrack/main.cpp" "/home/kingpalex/Uni/9517P/cuimg/samples/bundletrack/build3/CMakeFiles/ExploringSfMExec.dir/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "DISABLE_PCAP"
  "DISABLE_PNG"
  "USE_EIGEN"
  "NDEBUG"
  "NO_CUDA"
  "WITH_OPENCV"
  "EIGEN_USE_NEW_STDVECTOR"
  "EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET"
  "FLANN_STATIC"
  "qh_QHpointer"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/kingpalex/Uni/9517P/cuimg/samples/bundletrack/build3/CMakeFiles/ExploringSfMLibrary.dir/DependInfo.cmake"
  )
