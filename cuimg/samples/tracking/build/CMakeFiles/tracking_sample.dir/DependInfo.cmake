# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/kingpalex/Pro/cuimg/samples/tracking/main.cc" "/home/kingpalex/Pro/cuimg/samples/tracking/build/CMakeFiles/tracking_sample.dir/main.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "NDEBUG"
  "NO_CUDA"
  "WITH_OPENCV"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
