#include <iostream>
#include <vector>
#include <math.h>

#include <cuimg/profiler.h>
#include <cuimg/video_capture.h>
#include <cuimg/improved_builtin.h>
#include <cuimg/target.h>

#include <cuimg/copy.h>
#include <cuimg/dsl/all.h>
#include <cuimg/gpu/device_image2d.h>
#include <cuimg/cpu/host_image2d.h>

#include <cuimg/tracking2/tracker.h>

#include "opencv2/video/tracking.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cuimg;
using namespace cv;
using namespace std;

Mat K = (Mat_<double>(3,3) << 2.1193146663549155e+03, 0., 8.9090303824754801e+02, 0.,
       2.1184193020879648e+03, 5.7275927939885025e+02, 0., 0., 1.);
Mat Kt = K.t();

// trajectory store a short term trajectory.
struct trajectory
{
  trajectory() : alive(true) {}
  trajectory(i_int2 pos) : alive(true) { history.push_back(pos); }

  void move(trajectory&& t)
  {
    history.swap(t.history);
    alive = t.alive;
  }

  std::deque<i_int2> history;
  bool alive;
};


// Update a trajectory when a particle moves.
template <typename TR>
void update_trajectories(std::vector<trajectory>& v, TR& pset)
{
  const auto& parts = pset.dense_particles();
  for(unsigned i = 0; i < v.size(); i++)
    v[i].alive = false;
  for(unsigned i = 0; i < v.size(); i++)
    if (parts[i].age > 0)
    {
      assert(parts[i].age != 1 || v[i].history.empty());
      v[i].history.push_back(parts[i].pos);
      v[i].alive = true;
      if (v[i].history.size() > 20) v[i].history.pop_front();
    }
    else
    {
      v[i].history.clear();
    }
}



/**
 From "Triangulation", Hartley, R.I. and Sturm, P., Computer vision and image understanding, 1997
 */
Mat LinearLSTriangulation(Point3d u,       //homogenous image point (u,v,1)
                   const Matx34d& P,       //camera 1 matrix
                   Point3d u1,      //homogenous image point in 2nd camera
                   const Matx34d& P1       //camera 2 matrix
                                   )
{
    //build matrix A for homogenous equation system Ax = 0
    //assume X = (x,y,z,1), for Linear-LS method
    //which turns it into a AX = B system, where A is 4x3, X is 3x1 and B is 4x1
    Mat A = (Mat_<double>(4,3) <<
                     u.x*P(2,0)-P(0,0),    u.x*P(2,1)-P(0,1),      u.x*P(2,2)-P(0,2),
                     u.y*P(2,0)-P(1,0),    u.y*P(2,1)-P(1,1),      u.y*P(2,2)-P(1,2),
                     u1.x*P1(2,0)-P1(0,0), u1.x*P1(2,1)-P1(0,1),   u1.x*P1(2,2)-P1(0,2),
                     u1.y*P1(2,0)-P1(1,0), u1.y*P1(2,1)-P1(1,1),   u1.y*P1(2,2)-P1(1,2));
    Mat B = (Mat_<double>(4,1) << 
                   -(u.x*P(2,3)      -P(0,3)),
                   -(u.y*P(2,3)      -P(1,3)),
                   -(u1.x*P1(2,3)    -P1(0,3)),
                   -(u1.y*P1(2,3)    -P1(1,3)));

 
    Mat X(3,1,CV_32F);
    solve(A,B,X,DECOMP_SVD);
 
    return X;
}

//Triagulate points
void TriangulatePoints(const vector< Point2f >& pt_set1,
                       const vector< Point2f >& pt_set2,
                       const Matx33d& K,
                       const Matx34d& P,
                       const Matx34d& P1,
                       vector< Point3d >& pointcloud,
                       vector< Point2f >& correspImg1Pt,
		       vector< double >& depths)
{
    Mat Kinv(K.inv());
 
    pointcloud.clear();
    correspImg1Pt.clear();
 
    cout << "Triangulating...";
    double t = getTickCount();
    unsigned int pts_size = pt_set1.size();
#pragma omp parallel for
    for (unsigned int i=0; i<pts_size;i++) {
        Point2f kp = pt_set1[i];
        Point3d ut(kp.x,kp.y,1.0);
        Mat um = Kinv * Mat(ut);
        Point3d u(um);
        Point2f kp1 = pt_set2[i];
        Point3d ut1(kp1.x,kp1.y,1.0);
        Mat um1 = Kinv * Mat(ut1);
        Point3d u1(um1);
 
        //Mat_ X = IterativeLinearLSTriangulation(u,P,u1,P1);
        Mat X = LinearLSTriangulation(u,P,u1,P1);
 
//      if(X(2) > 6 || X(2) < 0) continue;
 
#pragma omp critical
        {
	    //cout << "X " << i << " " << X << endl;
            pointcloud.push_back(Point3d(X));
            correspImg1Pt.push_back(pt_set1[i]);
            depths.push_back(X.at<double>(2,0));
        }
    }
    t = ((double)getTickCount() - t)/getTickFrequency();
    cout << "Done. ("<< t << "). NumPoints (" << pts_size << ")" << endl;
}



int main(int argc, char* argv[])
{
  cv::VideoCapture video;

  if (argc == 4)
  {
    video.open(argv[1]);
  }
  else
  {
    std::cout << "Usage: ./tracking_qt video_file nscales detector_threshold" << std::endl;
    return -1;
  }

  if(!video.isOpened())
  {
    std::cout << "Cannot open " << argv[1] << std::endl;
    return -1;
  }

  int NSCALES = atoi(argv[2]);
  if (NSCALES <= 0 or NSCALES >= 10)
  {
    std::cout << "NSCALE should be > 0 and < 10, got " << argv[2] << std::endl;
    return -1;
  }

  int detector_threshold = atoi(argv[3]);

  obox2d domain(video.get(CV_CAP_PROP_FRAME_HEIGHT), video.get(CV_CAP_PROP_FRAME_WIDTH));
  host_image2d<gl8u> frame_gl(domain);

  // Tracker definition
  typedef tracker<tracking_strategies::bc2s_fast_gradient_cpu> T1;
  T1 tr1(domain, NSCALES);

  // Tracker settings
  tr1.strategy().set_detector_frequency(1);
  tr1.strategy().set_filtering_frequency(1);
  for (unsigned s = 0; s < NSCALES; s++)
    tr1.scale(s).strategy().detector().set_n(9).set_fast_threshold(detector_threshold);

  // Record trajectories at each scales.
  std::vector<std::vector<trajectory> > trajectories(NSCALES);

  cv::namedWindow("Out", CV_WINDOW_AUTOSIZE );


  cv::Mat input_;
  while (video.read(input_)) // For each frame
  {
    host_image2d<i_uchar3> frame(input_);
    frame_gl = get_x(frame); // Basic Gray level conversion.
    tr1.run(frame_gl);

    cv::Mat render;
    input_.copyTo(render);

    // reduce points to those that are matched
    vector< Point2f > imgpts1;
    vector< Point2f > imgpts2;

    for (unsigned s = 0; s < NSCALES; s++)
    {
      // Sync trajectories buffer with particles
      tr1.scale(s).pset().sync_attributes(trajectories[s], trajectory());
      // Update trajectories.
      update_trajectories(trajectories[s], tr1.scale(s).pset());

      // output trajectories
      for (int i=0;i<trajectories[s].size(); i++ ) {
        if(trajectories[s][i].alive && trajectories[s][i].history.size() >= 2) {
          std::deque<i_int2>::iterator it = trajectories[s][i].history.end();
	  it--;
	  int scal = pow(2,s);
	  cv::Point p((*it)[1] * scal,(*it)[0] * scal);
	  cv::circle(render,p,1, cv::Scalar(s*(255/(NSCALES-1)),255-s*(255/(NSCALES-1)),0), CV_FILLED);
	  it--;
	  cv::Point p2((*it)[1] * scal,(*it)[0] * scal);
          imgpts1.push_back(p);
          imgpts2.push_back(p2);
	}
      }
    }
    cv::imshow("Out" ,render);


      if (imgpts1.size() < 5) {
        cout << "Failed to find enough points" << endl;
        continue;
      }
      // Now try a 3D reconstruction
      vector< uchar > ffMStatus;
      Mat F = findFundamentalMat(imgpts1, imgpts2, CV_FM_RANSAC, 0.5, 0.999, ffMStatus);
      int ffMsum = std::accumulate( ffMStatus.begin(), ffMStatus.end(), 0 );

      // check F.
      double checkF = 0;
      int succeeded = 0;
      for (int i=0;i<imgpts1.size();i++) {
        if (ffMStatus[i]) {
          Point3d ut1(imgpts1[i].x,imgpts1[i].y,1.0);
          Point3d ut2(imgpts2[i].x,imgpts2[i].y,1.0);
          Mat c = Mat(ut2).t() *  F * Mat(ut1);
	  //cout << "Check at " << i << " is " << c  << endl;
	  checkF += abs(c.at<double>(0,0));
	  succeeded++;
	}
      }
      //cout << "Sum check " << checkF << ( (checkF < imgpts1.size()/2) ? "< " : ">= ") << imgpts1.size()/2 << endl;
      cout << "Sum check " << checkF << " vs " << succeeded << endl;
      // if the total error is small for each point
      //if (checkF < imgpts1.size()/2) {
      if (true) {

        // probably count the number of 1s in the ffMStatus array. If too low, this F is bad.
        Mat E = Kt * F * K;
        //Matx33d E = F;
        SVD svd(E); // later do it on K'*F*K
	// Check the svd?
        Mat checksvd = svd.u * Mat(Matx33d(1,0,0,0,1,0,0,0,0)) * svd.vt; //HZ 9.19
	cout << "check SVD " << E - checksvd << endl;
        Matx33d W(0,-1,0,   //HZ 9.13
                  1,0,0,
                  0,0,1);
        Matx33d Winv(0,1,0,
                    -1,0,0,
                    0,0,1);
        Mat R = svd.u * Mat(W) * svd.vt; //HZ 9.19
        Mat t = svd.u.col(2); //u3
        Matx34d P (1, 0, 0, 0,
                   0, 1, 0, 0,
  		   0, 0, 1, 0);
                   
        Matx34d P1(R.at<double>(0,0),    R.at<double>(0,1), R.at<double>(0,2), t.at<double>(0),
                   R.at<double>(1,0),    R.at<double>(1,1), R.at<double>(1,2), t.at<double>(1),
                   R.at<double>(2,0),    R.at<double>(2,1), R.at<double>(2,2), t.at<double>(2));
        
        vector< Point3d > pointcloud;
        vector< Point2f > correspImg1Pt;
  
        vector< double > depths;
        TriangulatePoints(imgpts1,imgpts2,
                         K,
                         P,
                         P1,
                         pointcloud,
                         correspImg1Pt,
  		       depths);
  
        double minVal,maxVal;
        minMaxLoc(depths, &minVal, &maxVal);
        cv::Mat tmp;//(240,320,CV_8UC3);
        cvtColor(input_, tmp, CV_BGR2HSV);
        for (unsigned int i=0; i<pointcloud.size(); i++) {
            double _d = MAX(MIN((pointcloud[i].z-minVal)/(maxVal-minVal),1.0),0.0);
            circle(tmp, correspImg1Pt[i], 1, Scalar(255 * (1.0-(_d)),255,255), CV_FILLED);
        }
        cvtColor(tmp, tmp, CV_HSV2BGR);
        imshow("Triangulation", tmp);


        // Render Top Down Map
        Mat map = Mat::zeros(Size(500,500),CV_8UC3);
        for (unsigned int i=0; i<pointcloud.size(); i++) {
            double _d = MAX(MIN((pointcloud[i].y-minVal)/(maxVal-minVal),1.0),0.0);
	    Point2f topdown(250+100*pointcloud[i].x,250+100*pointcloud[i].z);
            circle(map, topdown, 1, Scalar(255 * (1.0-(_d)),255,255), CV_FILLED);
        }
        imshow("SLAM_TOP", map);
        // Render Side Map
        Mat side = Mat::zeros(Size(500,500),CV_8UC3);
        for (unsigned int i=0; i<pointcloud.size(); i++) {
            double _d = MAX(MIN((pointcloud[i].x-minVal)/(maxVal-minVal),1.0),0.0);
	    Point2f sideon(250+100*pointcloud[i].z,250+100*pointcloud[i].y);
            circle(side, sideon, 1, Scalar(255 * (1.0-(_d)),255,255), CV_FILLED);
        }
        imshow("SLAM_SIDE", side);
    }

    cv::waitKey(10);
    
  }

  return 0;
}
