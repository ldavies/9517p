#include <iostream>
#include <fstream>

#include "opencv2/video/tracking.hpp"
#include "opencv2/highgui/highgui.hpp"

#define MAXWIDTH 150.0

using namespace cv;
using namespace std;

inline bool isFlowCorrect(Point2f u)
{
    return !cvIsNaN(u.x) && !cvIsNaN(u.y) && fabs(u.x) < 1e9 && fabs(u.y) < 1e9;
}

static Vec3b computeColor(float fx, float fy)
{
    static bool first = true;

    // relative lengths of color transitions:
    // these are chosen based on perceptual similarity
    // (e.g. one can distinguish more shades between red and yellow
    //  than between yellow and green)
    const int RY = 15;
    const int YG = 6;
    const int GC = 4;
    const int CB = 11;
    const int BM = 13;
    const int MR = 6;
    const int NCOLS = RY + YG + GC + CB + BM + MR;
    static Vec3i colorWheel[NCOLS];

    if (first)
    {
        int k = 0;

        for (int i = 0; i < RY; ++i, ++k)
            colorWheel[k] = Vec3i(255, 255 * i / RY, 0);

        for (int i = 0; i < YG; ++i, ++k)
            colorWheel[k] = Vec3i(255 - 255 * i / YG, 255, 0);

        for (int i = 0; i < GC; ++i, ++k)
            colorWheel[k] = Vec3i(0, 255, 255 * i / GC);

        for (int i = 0; i < CB; ++i, ++k)
            colorWheel[k] = Vec3i(0, 255 - 255 * i / CB, 255);

        for (int i = 0; i < BM; ++i, ++k)
            colorWheel[k] = Vec3i(255 * i / BM, 0, 255);

        for (int i = 0; i < MR; ++i, ++k)
            colorWheel[k] = Vec3i(255, 0, 255 - 255 * i / MR);

        first = false;
    }

    const float rad = sqrt(fx * fx + fy * fy);
    const float a = atan2(-fy, -fx) / (float)CV_PI;

    const float fk = (a + 1.0f) / 2.0f * (NCOLS - 1);
    const int k0 = static_cast<int>(fk);
    const int k1 = (k0 + 1) % NCOLS;
    const float f = fk - k0;

    Vec3b pix;

    for (int b = 0; b < 3; b++)
    {
        const float col0 = colorWheel[k0][b] / 255.f;
        const float col1 = colorWheel[k1][b] / 255.f;

        float col = (1 - f) * col0 + f * col1;

        if (rad <= 1)
            col = 1 - rad * (1 - col); // increase saturation with radius
        else
            col *= .75; // out of range

        pix[2 - b] = static_cast<uchar>(255.f * col);
    }

    return pix;
}

static void drawOpticalFlow(const Mat_<Point2f>& flow, Mat& dst, float maxmotion = -1)
{
    dst.create(flow.size(), CV_8UC3);
    dst.setTo(Scalar::all(0));

    // determine motion range:
    float maxrad = maxmotion;

    if (maxmotion <= 0)
    {
        maxrad = 1;
        for (int y = 0; y < flow.rows; ++y)
        {
            for (int x = 0; x < flow.cols; ++x)
            {
                Point2f u = flow(y, x);

                if (!isFlowCorrect(u))
                    continue;

                maxrad = max(maxrad, sqrt(u.x * u.x + u.y * u.y));
            }
        }
    }

    for (int y = 0; y < flow.rows; ++y)
    {
        for (int x = 0; x < flow.cols; ++x)
        {
            Point2f u = flow(y, x);

            if (isFlowCorrect(u))
                dst.at<Vec3b>(y, x) = computeColor(u.x / maxrad, u.y / maxrad);
        }
    }
}

// binary file format for flow data specified here:
// http://vision.middlebury.edu/flow/data/
static void writeOpticalFlowToFile(const Mat_<Point2f>& flow, const string& fileName)
{
    static const char FLO_TAG_STRING[] = "PIEH";

    ofstream file(fileName.c_str(), ios_base::binary);

    file << FLO_TAG_STRING;

    file.write((const char*) &flow.cols, sizeof(int));
    file.write((const char*) &flow.rows, sizeof(int));

    for (int i = 0; i < flow.rows; ++i)
    {
        for (int j = 0; j < flow.cols; ++j)
        {
            const Point2f u = flow(i, j);

            file.write((const char*) &u.x, sizeof(float));
            file.write((const char*) &u.y, sizeof(float));
        }
    }
}

void getLeftStereoFrame(Mat& in, Mat& out) {
  int cols = in.cols/2;
  int outrows = in.rows/2;
  int croprows = in.rows;
  resize(in(Rect(0,0,cols,croprows)),out,Size(cols,outrows));
}

int other(int cf) { return 1-cf; }

// takes input p1 and p2 and merges both into p2.
// comparison checks distance between points.
// if below threshold, points are deemed identical and merged.
int mergePoints(vector<Point2f>& p1, vector<uchar> include, vector<Point2f>& p2, float threshold=2) {
  int numMerges = 0;
  for (int i=0; i<p1.size(); i++) {
    if (!include[i]) continue;
    float dp1p2=100;
    float ip1p2=-1;
    for (int j=0; j<p2.size(); j++) {
      float dist = sqrt((p1[i].x-p2[j].x)*(p1[i].x-p2[j].x) + (p1[i].y-p2[j].y)*(p1[i].y-p2[j].y));
      if (dist < dp1p2) {
        dp1p2 = dist;
	ip1p2 = j;
      }
    }
    if (ip1p2 == -1 || dp1p2 > threshold) p2.push_back(p1[i]);
    else numMerges++;
  }
}

int main(int argc, const char* argv[])
{
    if (argc < 2)
    {
        cerr << "Usage : " << argv[0] << "<video>" << endl;
        return -1;
    }

    VideoCapture capture(argv[1]);
        if (!capture.isOpened())
          {
            cerr << "Failed to open " << argv[1] << endl;
	    exit(0);
          }

    int curframe = 0;
    int frames = 0;
    Mat frame[2];
    Mat bwframe[2];

    vector< Point2f >  corners[2];
    vector< Point2f >  corners_est[2];

    namedWindow( "Flow", CV_WINDOW_AUTOSIZE );
    namedWindow( "VP", CV_WINDOW_AUTOSIZE );

    while (true) {
      frames++;

      capture >> frame[curframe];

      if (frame[curframe].empty()) {
        cout << "Video finished" << endl;
        break;
      }

      getLeftStereoFrame(frame[curframe],frame[curframe]);

      cvtColor(frame[curframe], bwframe[curframe], CV_BGR2GRAY);

      vector<uchar> of_status;
      vector<float> of_error; // may be error, not char

      goodFeaturesToTrack(bwframe[curframe],
                        corners[curframe],
                        50,
			0.01,
                        2,
                        noArray(),
                        3,
                        true,
                        0.04 );

      cout << "Feature track frame " << frames << endl;
      if (frames<=1) {
        curframe = other(curframe);
        continue;
      }

      const double start = (double)getTickCount();
      calcOpticalFlowPyrLK(   bwframe[other(curframe)], 
                              bwframe[curframe], 
			      corners[other(curframe)], 
			      corners_est[curframe], 
			      of_status,
			      of_error,
			      Size(21,21), //size of pyramids
			      3, //maxlevel 
			      TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 0.01), 
			      OPTFLOW_LK_GET_MIN_EIGENVALS, //flags
			      5e-3 // min eig threshold
			      );
      const double timeSec = (getTickCount() - start) / getTickFrequency();
      cout << "calcOpticalFlowPyrLK : " << timeSec << " sec" << endl;

      Mat out;
      frame[curframe].copyTo(out);
      Mat VP = Mat::zeros(out.size(),CV_8U);

      int fails = 0;
      // draw features
      for (int i=0; i< corners[curframe].size();i++) circle(out, corners[curframe][i], 6, Scalar(0,255,255),2,8);
      for (int i=0; i< corners_est[curframe].size();i++) {
        if (of_status[i]) circle(out, corners_est[curframe][i], 6, Scalar(255,255,0),2,8); 
        else fails++;
      }
      
      int numMerges = mergePoints(corners_est[curframe],of_status,corners[curframe],4);

      cout << "Fails = " << fails << endl;
      cout << "numMerges = " << numMerges << endl;

      // draw lines from corners[other(curframe)] to corners_est[curframe]
      for (int i=0; i< corners_est[curframe].size();i++) {
        if (of_status[i] && of_error[i] >= 0.01) {
	  Point2f d = corners_est[curframe][i] - corners[other(curframe)][i];
	  double movement = sqrt(d.x*d.x + d.y*d.y);
	  // if the movement is too small, cull it.
	  if (movement > 1) {
            double weight = 2* log(1+movement + 10*of_error[i]);
            double thickness = 0.5/of_error[i];
            cout << "OF line moved = " << movement << " weight = " << weight << " thickness = " << thickness << " of_error = " << of_error[i] << endl;
            // project to edges
            Point start = corners[other(curframe)][i] - (d*1000);
            Point end = corners[other(curframe)][i] + (d*1000);
            if (clipLine(VP.size(),start,end)) {
                  Mat opt = Mat::zeros(VP.size(),CV_8U);
                  line(opt,start,end,Scalar(weight,weight,weight),thickness,CV_AA);
              VP += opt;
            } 
            else
              cout << "Line " << i << " is completely outside image rect" << endl;
          }
	}
      }
      // get max
      Point max;
      minMaxLoc(VP,0,0,0,&max);
      circle(out,max,6,Scalar(0,0,255),3,8);


      imshow("Flow", out);
      imshow("VP", VP);
      curframe = other(curframe);
      waitKey(1000);
    }
    waitKey();

    return 0;
}

/*
    int buildOpticalFlowPyramid(InputArray img, 
                                OutputArrayOfArrays pyramid, 
				Size winSize, 
				int maxLevel, 
				bool withDerivatives=true, 
				int pyrBorder=BORDER_REFLECT_101, 
				int derivBorder=BORDER_CONSTANT, 
				bool tryReuseInputImage=true);
     double calcGlobalOrientation(InputArray orientation, 
                                  InputArray mask, 
				  InputArray mhi, 
				  double timestamp, 
				  double duration);

*/
