#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>

#include "opencv2/video/tracking.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#define MAXWIDTH 150.0

#define __SFM__DEBUG__

using namespace cv;
using namespace std;

//Matx33d K(1,0,0,0,1,0,0,0,1);

//Mat K = (Mat_<double>(3,3) << 1,0,0,0,1,0,0,0,1);
Mat K = (Mat_<double>(3,3) << 2.1193146663549155e+03, 0., 8.9090303824754801e+02, 0.,
       2.1184193020879648e+03, 5.7275927939885025e+02, 0., 0., 1.);
Mat Kt = K.t();

inline bool isFlowCorrect(Point2f u)
{
    return !cvIsNaN(u.x) && !cvIsNaN(u.y) && fabs(u.x) < 1e9 && fabs(u.y) < 1e9;
}

static Vec3b computeColor(float fx, float fy)
{
    static bool first = true;

    // relative lengths of color transitions:
    // these are chosen based on perceptual similarity
    // (e.g. one can distinguish more shades between red and yellow
    //  than between yellow and green)
    const int RY = 15;
    const int YG = 6;
    const int GC = 4;
    const int CB = 11;
    const int BM = 13;
    const int MR = 6;
    const int NCOLS = RY + YG + GC + CB + BM + MR;
    static Vec3i colorWheel[NCOLS];

    if (first)
    {
        int k = 0;

        for (int i = 0; i < RY; ++i, ++k)
            colorWheel[k] = Vec3i(255, 255 * i / RY, 0);

        for (int i = 0; i < YG; ++i, ++k)
            colorWheel[k] = Vec3i(255 - 255 * i / YG, 255, 0);

        for (int i = 0; i < GC; ++i, ++k)
            colorWheel[k] = Vec3i(0, 255, 255 * i / GC);

        for (int i = 0; i < CB; ++i, ++k)
            colorWheel[k] = Vec3i(0, 255 - 255 * i / CB, 255);

        for (int i = 0; i < BM; ++i, ++k)
            colorWheel[k] = Vec3i(255 * i / BM, 0, 255);

        for (int i = 0; i < MR; ++i, ++k)
            colorWheel[k] = Vec3i(255, 0, 255 - 255 * i / MR);

        first = false;
    }

    const float rad = sqrt(fx * fx + fy * fy);
    const float a = atan2(-fy, -fx) / (float)CV_PI;

    const float fk = (a + 1.0f) / 2.0f * (NCOLS - 1);
    const int k0 = static_cast<int>(fk);
    const int k1 = (k0 + 1) % NCOLS;
    const float f = fk - k0;

    Vec3b pix;

    for (int b = 0; b < 3; b++)
    {
        const float col0 = colorWheel[k0][b] / 255.f;
        const float col1 = colorWheel[k1][b] / 255.f;

        float col = (1 - f) * col0 + f * col1;

        if (rad <= 1)
            col = 1 - rad * (1 - col); // increase saturation with radius
        else
            col *= .75; // out of range

        pix[2 - b] = static_cast<uchar>(255.f * col);
    }

    return pix;
}

static void drawOpticalFlow(const Mat_<Point2f>& flow, Mat& dst, float maxmotion = -1)
{
    dst.create(flow.size(), CV_8UC3);
    dst.setTo(Scalar::all(0));

    // determine motion range:
    float maxrad = maxmotion;

    if (maxmotion <= 0)
    {
        maxrad = 1;
        for (int y = 0; y < flow.rows; ++y)
        {
            for (int x = 0; x < flow.cols; ++x)
            {
                Point2f u = flow(y, x);

                if (!isFlowCorrect(u))
                    continue;

                maxrad = max(maxrad, sqrt(u.x * u.x + u.y * u.y));
            }
        }
    }

    for (int y = 0; y < flow.rows; ++y)
    {
        for (int x = 0; x < flow.cols; ++x)
        {
            Point2f u = flow(y, x);

            if (isFlowCorrect(u))
                dst.at<Vec3b>(y, x) = computeColor(u.x / maxrad, u.y / maxrad);
        }
    }
}
// binary file format for flow data specified here:
// http://vision.middlebury.edu/flow/data/
static void writeOpticalFlowToFile(const Mat_<Point2f>& flow, const string& fileName)
{
    static const char FLO_TAG_STRING[] = "PIEH";

    ofstream file(fileName.c_str(), ios_base::binary);

    file << FLO_TAG_STRING;

    file.write((const char*) &flow.cols, sizeof(int));
    file.write((const char*) &flow.rows, sizeof(int));

    for (int i = 0; i < flow.rows; ++i)
    {
        for (int j = 0; j < flow.cols; ++j)
        {
            const Point2f u = flow(i, j);

            file.write((const char*) &u.x, sizeof(float));
            file.write((const char*) &u.y, sizeof(float));
        }
    }
}


static void drawOptFlowMap(const Mat& flow, Mat& cflowmap, int step,
                    double scale, const Scalar& color)
{
    for(int y = 0; y < cflowmap.rows; y += step)
        for(int x = 0; x < cflowmap.cols; x += step)
        {
            const Point2f& fxy = flow.at<Point2f>(y, x) * scale;
            line(cflowmap, Point(x,y), Point(cvRound(x+fxy.x), cvRound(y+fxy.y)),
                 color);
            circle(cflowmap, Point(x,y), 1, color, -1);
        }
}
void getLeftStereoFrame(Mat& in, Mat& out) {
  int cols = in.cols/2;
  //int outrows = in.rows/2;
  int outrows = in.rows;
  int croprows = in.rows;
  resize(in(Rect(0,0,cols,croprows)),out,Size(cols,outrows));
}

int other(int cf) { return 1-cf; }

// takes input p1 and p2 and merges both into p2.
// comparison checks distance between points.
// if below threshold, points are deemed identical and merged.
int mergePoints(vector<Point2f>& p1, vector<uchar> include, vector<Point2f>& p2, float threshold=2) {
  int numMerges = 0;
  for (int i=0; i<p1.size(); i++) {
    if (!include[i]) continue;
    float dp1p2=100;
    float ip1p2=-1;
    for (int j=0; j<p2.size(); j++) {
      float dist = sqrt((p1[i].x-p2[j].x)*(p1[i].x-p2[j].x) + (p1[i].y-p2[j].y)*(p1[i].y-p2[j].y));
      if (dist < dp1p2) {
        dp1p2 = dist;
	ip1p2 = j;
      }
    }
    if (ip1p2 == -1 || dp1p2 > threshold) p2.push_back(p1[i]);
    else numMerges++;
  }
}

/**
 From "Triangulation", Hartley, R.I. and Sturm, P., Computer vision and image understanding, 1997
 */
Mat LinearLSTriangulation(Point3d u,       //homogenous image point (u,v,1)
                   const Matx34d& P,       //camera 1 matrix
                   Point3d u1,      //homogenous image point in 2nd camera
                   const Matx34d& P1       //camera 2 matrix
                                   )
{
    //build matrix A for homogenous equation system Ax = 0
    //assume X = (x,y,z,1), for Linear-LS method
    //which turns it into a AX = B system, where A is 4x3, X is 3x1 and B is 4x1
    Mat A = (Mat_<double>(4,3) <<
                     u.x*P(2,0)-P(0,0),    u.x*P(2,1)-P(0,1),      u.x*P(2,2)-P(0,2),
                     u.y*P(2,0)-P(1,0),    u.y*P(2,1)-P(1,1),      u.y*P(2,2)-P(1,2),
                     u1.x*P1(2,0)-P1(0,0), u1.x*P1(2,1)-P1(0,1),   u1.x*P1(2,2)-P1(0,2),
                     u1.y*P1(2,0)-P1(1,0), u1.y*P1(2,1)-P1(1,1),   u1.y*P1(2,2)-P1(1,2));
    Mat B = (Mat_<double>(4,1) << -(u.x*P(2,3)      -P(0,3)),
                   -(u.y*P(2,3)      -P(1,3)),
                   -(u1.x*P1(2,3)    -P1(0,3)),
                   -(u1.y*P1(2,3)    -P1(1,3)));

 
    Mat X(3,1,CV_32F);
    solve(A,B,X,DECOMP_SVD);
 
    return X;
}

//Triagulate points
void TriangulatePoints(const vector< Point2f >& pt_set1,
                       const vector< Point2f >& pt_set2,
                       const Matx33d& K,
                       const Matx34d& P,
                       const Matx34d& P1,
                       vector< Point3d >& pointcloud,
                       vector< Point2f >& correspImg1Pt,
		       vector< double >& depths)
{
    Mat Kinv(K.inv());
 
    pointcloud.clear();
    correspImg1Pt.clear();
 
    cout << "Triangulating...";
    double t = getTickCount();
    unsigned int pts_size = pt_set1.size();
#pragma omp parallel for
    for (unsigned int i=0; i<pts_size;i++) {
        Point2f kp = pt_set1[i];
        Point3d ut(kp.x,kp.y,1.0);
        Mat um = Kinv * Mat(ut);
        Point3d u(um);
        Point2f kp1 = pt_set2[i];
        Point3d ut1(kp1.x,kp1.y,1.0);
        Mat um1 = Kinv * Mat(ut1);
        Point3d u1(um1);
 
        //Mat_ X = IterativeLinearLSTriangulation(u,P,u1,P1);
        Mat X = LinearLSTriangulation(u,P,u1,P1);
 
//      if(X(2) > 6 || X(2) < 0) continue;
 
#pragma omp critical
        {
	    //cout << "X " << i << " " << X << endl;
            pointcloud.push_back(Point3d(X));
            correspImg1Pt.push_back(pt_set1[i]);
#ifdef __SFM__DEBUG__
            depths.push_back(X.at<double>(2,0));
#endif
        }
    }
    t = ((double)getTickCount() - t)/getTickFrequency();
    cout << "Done. ("<< t << "). NumPoints (" << pts_size << ")" << endl;
}

void blurFlow(Mat& flow) {
  Mat xy[2]; //X,Y
  split(flow, xy);
  xy[0].convertTo(xy[0],CV_32F);
  xy[1].convertTo(xy[1],CV_32F);
  imshow("X",xy[0]);
  imshow("Y",xy[1]);
  medianBlur(xy[0],xy[0],5);
  medianBlur(xy[1],xy[1],5);
  merge(xy,2,flow);
  //GaussianBlur( inflow, flow, Size( 91, 91 ), 0, 0 );
}

int main(int argc, const char* argv[])
{
    if (argc < 2)
    {
        cerr << "Usage : " << argv[0] << "<video>" << endl;
        return -1;
    }

    VideoCapture capture(argv[1]);
        if (!capture.isOpened())
          {
            cerr << "Failed to open " << argv[1] << endl;
	    exit(0);
          }

    capture.set(CV_CAP_PROP_POS_FRAMES,1200);

    int curframe = 0;
    int frames = 0;
    Mat frame[2];
    Mat bwframe[2];

    vector< Point2f >  corners[2];
    vector< Point2f >  corners_est[2];

    namedWindow( "Flow", CV_WINDOW_AUTOSIZE );
    namedWindow( "BlurFlow", CV_WINDOW_AUTOSIZE );
    namedWindow( "FP", CV_WINDOW_AUTOSIZE );
    //namedWindow( "VP", CV_WINDOW_AUTOSIZE );
    namedWindow( "Triangulation", CV_WINDOW_AUTOSIZE );
    namedWindow( "SLAM_TOP", CV_WINDOW_AUTOSIZE );
    namedWindow( "SLAM_SIDE", CV_WINDOW_AUTOSIZE );
    namedWindow( "X", CV_WINDOW_AUTOSIZE );
    namedWindow( "Y", CV_WINDOW_AUTOSIZE );

    Mat farneback_flow;

    Ptr<DenseOpticalFlow> tvl1 = createOptFlow_DualTVL1();

    while (true) {
      frames++;

      Mat bullshit;
      capture >> bullshit;

      if (bullshit.empty()) {
        cout << "Video finished" << endl;
        break;
      }
      bullshit.copyTo(frame[curframe]);

      getLeftStereoFrame(frame[curframe],frame[curframe]);

      cvtColor(frame[curframe], bwframe[curframe], CV_BGR2GRAY);
      equalizeHist( bwframe[curframe], bwframe[curframe] );

      vector<uchar> of_status;
      vector<float> of_error; // may be error, not char

      goodFeaturesToTrack(bwframe[curframe],
                        corners[curframe],
                        1000,
			0.01,
                        2,
                        noArray(),
                        3,
                        true,
                        0.04 );

      if (frames<=1) {
        curframe = other(curframe);
        continue;
      }

      const double start = (double)getTickCount();
      calcOpticalFlowPyrLK(   bwframe[other(curframe)], 
                              bwframe[curframe], 
			      corners[other(curframe)], 
			      corners_est[curframe], 
			      of_status,
			      of_error,
			      Size(21,21), //size of pyramids
			      3, //maxlevel 
			      TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 0.01), 
			      OPTFLOW_LK_GET_MIN_EIGENVALS, //flags
			      5e-3 // min eig threshold
			      );
      const double timeSec = (getTickCount() - start) / getTickFrequency();
      cout << "calcOpticalFlowPyrLK : " << timeSec << " sec" << endl;

      Mat out;
      frame[curframe].copyTo(out);
      Mat VP = Mat::zeros(out.size(),CV_8U);

      int fails = 0;
      // draw features
      for (int i=0; i< corners[curframe].size();i++) circle(out, corners[curframe][i], 6, Scalar(0,255,255),2,8);
      for (int i=0; i< corners_est[curframe].size();i++) {
        if (of_status[i]) circle(out, corners_est[curframe][i], 6, Scalar(255,255,0),2,8); 
        else fails++;
      }
      
      int numMerges = mergePoints(corners_est[curframe],of_status,corners[curframe],4);

      cout << "Fails = " << fails << endl;
      cout << "numMerges = " << numMerges << endl;

/*
      // draw lines from corners[other(curframe)] to corners_est[curframe]
      for (int i=0; i< corners_est[curframe].size();i++) {
        if (of_status[i] && of_error[i] >= 0.01) {
	  Point2f d = corners_est[curframe][i] - corners[other(curframe)][i];
	  double movement = sqrt(d.x*d.x + d.y*d.y);
	  // if the movement is too small, cull it.
	  if (movement > 1) {
            double weight = 2* log(1+movement + 10*of_error[i]);
            double thickness = 0.5/of_error[i];
            //cout << "OF line moved = " << movement << " weight = " << weight << " thickness = " << thickness << " of_error = " << of_error[i] << endl;
            // project to edges
            Point start = corners[other(curframe)][i] - (d*1000);
            Point end = corners[other(curframe)][i] + (d*1000);
            if (clipLine(VP.size(),start,end)) {
                  Mat opt = Mat::zeros(VP.size(),CV_8U);
                  line(opt,start,end,Scalar(weight,weight,weight),thickness,CV_AA);
              VP += opt;
            } 
            else
              cout << "Line " << i << " is completely outside image rect" << endl;
          }
	}
      }
      // get max
      Point max;
      minMaxLoc(VP,0,0,0,&max);
      circle(out,max,6,Scalar(0,0,255),3,8);
*/
      // get OF using another method
      int fflags = OPTFLOW_FARNEBACK_GAUSSIAN;
      if (frames > 3) {
        //fflags = fflags | OPTFLOW_USE_INITIAL_FLOW;
	// blur the old flow
//	blurFlow(blur_flow);
      }
      calcOpticalFlowFarneback(bwframe[other(curframe)], bwframe[curframe], farneback_flow, 0.5, 3, 15, 3, 7, 1.5, fflags);
      Mat col_flow;
      cvtColor(bwframe[other(curframe)], col_flow, CV_GRAY2BGR);
      drawOptFlowMap(farneback_flow, col_flow,8,2, CV_RGB(0,255,0));

      // test blurred flow
      Mat blur_flow;
      farneback_flow.copyTo(blur_flow);
      /*
      GaussianBlur( farneback_flow, blur_flow, Size( 31, 31 ),5,5);
      //medianBlur( farneback_flow, blur_flow, 31);
      */
      blurFlow(blur_flow);
      Mat col_flow2;
      cvtColor(bwframe[other(curframe)], col_flow2, CV_GRAY2BGR);
      drawOptFlowMap(blur_flow, col_flow2,8,2, CV_RGB(0,255,0));
      
      //drawOpticalFlow(blur_flow, col_flow2);

      imshow("Flow", col_flow);
      imshow("BlurFlow", col_flow2);
      // reduce points to those that are matched
      vector< Point2f > imgpts1;
      vector< Point2f > imgpts2;
      for (int i=0;i<of_status.size();i++) if(of_status[i]) {
        imgpts1.push_back(corners[other(curframe)][i]);
        imgpts2.push_back(corners_est[curframe][i]);
      }

      // add the remaining large movement OF vectors from the Farneback method

      for(int y = 0; y < blur_flow.rows; y++)

        for(int x = 0; x < blur_flow.cols; x++)
        {
            Point2f& fxy = blur_flow.at<Point2f>(y, x);
            if (abs(fxy.x) >= 1) {
	      fxy.x += x;
	      fxy.y += y;
	      imgpts1.push_back(Point(x,y));
	      imgpts2.push_back(fxy);
	    }
        }

      // Now try a 3D reconstruction
      vector< uchar > ffMStatus;
      Mat F = findFundamentalMat(imgpts1, imgpts2, CV_FM_RANSAC, 0.5, 0.999, ffMStatus);
      int ffMsum = std::accumulate( ffMStatus.begin(), ffMStatus.end(), 0 );
      // probably count the number of 1s in the ffMStatus array. If too low, this F is bad.
      Mat E = Kt * F * K;
      //Matx33d E = F;
      SVD svd(F); // later do it on K'*F*K
      Matx33d W(0,-1,0,   //HZ 9.13
            1,0,0,
            0,0,1);
      Matx33d Winv(0,1,0,
           -1,0,0,
           0,0,1);
      Mat R = svd.u * Mat(W) * svd.vt; //HZ 9.19
      Mat t = svd.u.col(2); //u3
      Matx34d P (1, 0, 0, 0,
                 0, 1, 0, 0,
		 0, 0, 1, 0);
                 
      Matx34d P1(R.at<double>(0,0),    R.at<double>(0,1), R.at<double>(0,2), t.at<double>(0),
                 R.at<double>(1,0),    R.at<double>(1,1), R.at<double>(1,2), t.at<double>(1),
                 R.at<double>(2,0),    R.at<double>(2,1), R.at<double>(2,2), t.at<double>(2));
      
      vector< Point3d > pointcloud;
      vector< Point2f > correspImg1Pt;

#ifdef __SFM__DEBUG__
    vector< double > depths;
#endif
      TriangulatePoints(imgpts1,imgpts2,
                       K,
                       P,
                       P1,
                       pointcloud,
                       correspImg1Pt,
		       depths);

    //show "range image"
#ifdef __SFM__DEBUG__
    {
        double minVal,maxVal;
        minMaxLoc(depths, &minVal, &maxVal);
        Mat tmp;//(240,320,CV_8UC3);
        cvtColor(frame[curframe], tmp, CV_BGR2HSV);
        for (unsigned int i=0; i<pointcloud.size(); i++) {
            double _d = MAX(MIN((pointcloud[i].z-minVal)/(maxVal-minVal),1.0),0.0);
            circle(tmp, correspImg1Pt[i], 1, Scalar(255 * (1.0-(_d)),255,255), CV_FILLED);
        }
        cvtColor(tmp, tmp, CV_HSV2BGR);
        imshow("Triangulation", tmp);


        // Render Top Down Map
        Mat map = Mat::zeros(Size(500,500),CV_8UC3);
        for (unsigned int i=0; i<pointcloud.size(); i++) {
            double _d = MAX(MIN((pointcloud[i].z-minVal)/(maxVal-minVal),1.0),0.0);
	    Point2f topdown(250+100*pointcloud[i].x,250+100*pointcloud[i].z);
            circle(map, topdown, 1, Scalar(255 * (1.0-(_d)),255,255), CV_FILLED);
        }
        imshow("SLAM_TOP", map);
        // Render Side Map
        Mat side = Mat::zeros(Size(500,500),CV_8UC3);
        for (unsigned int i=0; i<pointcloud.size(); i++) {
            double _d = MAX(MIN((pointcloud[i].z-minVal)/(maxVal-minVal),1.0),0.0);
	    Point2f sideon(250+100*pointcloud[i].z,250+100*pointcloud[i].y);
            circle(side, sideon, 1, Scalar(255 * (1.0-(_d)),255,255), CV_FILLED);
        }
        imshow("SLAM_SIDE", side);

	cout << "F" << endl << F << endl;
	cout << "P1" << endl << P1 << endl;
	cout << "ffm sum " <<  ffMsum << endl;
    }
#endif

      imshow("FP", out);
      //imshow("VP", VP);
      curframe = other(curframe);
      //waitKey(1000);
      waitKey(5);
    }
    waitKey();

    return 0;
}

/*
    int buildOpticalFlowPyramid(InputArray img, 
                                OutputArrayOfArrays pyramid, 
				Size winSize, 
				int maxLevel, 
				bool withDerivatives=true, 
				int pyrBorder=BORDER_REFLECT_101, 
				int derivBorder=BORDER_CONSTANT, 
				bool tryReuseInputImage=true);
     double calcGlobalOrientation(InputArray orientation, 
                                  InputArray mask, 
				  InputArray mhi, 
				  double timestamp, 
				  double duration);

*/
