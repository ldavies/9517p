#include "TemplateImage.hpp"

#define NUM_PYRAMIDS 5
#define DOWN_SCALER 1.5

TemplateImage::TemplateImage( Mat& t, featureMatcher * m)
  {
    if (!t.data) throw cv::Exception(1, string("Cannot create template: No image data."), NULL, NULL, 0);
    if (m == NULL) throw cv::Exception(1, string("No matcher specified."), NULL, NULL, 0);

    Mat data;
    Mat tmp;
    vector< KeyPoint > k;
    _matcher = m;
    
    computeData(t,data,k);
    _original = new Pyramid(t,data,k,1.0,&_allKeypoints);

    float divider = pow(DOWN_SCALER,NUM_PYRAMIDS); 

    int rowmult = (int)((float)t.rows/divider + 0.5);
    int colmult = (int)((float)t.cols/divider + 0.5);
    float rows = rowmult * divider;
    float cols = colmult * divider;
    float ratio = max(rows / t.rows, cols / t.cols);
    Mat tmp2;
    t.copyTo(tmp2);
    resize(t,tmp2,Size(t.cols * ratio * DOWN_SCALER, t.rows * ratio * DOWN_SCALER));
    tmp = tmp2(Rect(0,0,cols*DOWN_SCALER,rows*DOWN_SCALER));

    // three pyramids
    for (int i=0; i< NUM_PYRAMIDS; i++)
      {
        Mat out;
        vector<KeyPoint> k;
        //pyrDown(tmp,out,Size(cols,rows));
	resize(tmp,out,Size(cols,rows));
	computeData(out,data,k);
        Pyramid p(out,data,k,ratio,&_allKeypoints);
	_pyramids.push_back(p);
	rows/=DOWN_SCALER;cols/=DOWN_SCALER;ratio/=DOWN_SCALER;
	out.copyTo(tmp);
      }
    // test upsample later.
    // create ROI extractor
    _roiExtractor = new ROIExtractor(_original->_image);

  }

TemplateImage::~TemplateImage()
  {
    delete _original;
    delete _roiExtractor;
  }
