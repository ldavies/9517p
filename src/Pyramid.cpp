#include "Pyramid.hpp"

Pyramid::Pyramid(Mat image, Mat data, vector< KeyPoint > kps, float ratio, vector< KeyPoint > * kp)
{
   _image = image;
   _data = data;
   _keypoints = kps;
   _ratio = ratio;
   _kpIndex = kp->size();

   vector< KeyPoint > scKP = _keypoints;
   for (int i=0; i<scKP.size(); i++)
   {
     // scale kp from pyramid to original
     // hope this works
     scKP[i].pt.x /= ratio;
     scKP[i].pt.y /= ratio;
   }
   kp->insert(kp->end(), scKP.begin(), scKP.end());
}
