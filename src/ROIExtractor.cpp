//extract roi based on HSV blobs

#include "ROIExtractor.hpp"

namespace
{
   void HSVwLim(Mat &img){
      Mat overmask,undermask;
      vector<Mat> chans;
      int cLow = 100;
      int cHigh = 255;
      cvtColor(img,img,CV_RGB2HSV);
      split(img,chans);
      threshold(chans[2],overmask,cLow,255,CV_THRESH_BINARY);
      threshold(chans[2],undermask,cHigh,255,CV_THRESH_BINARY_INV);
      chans[2] =  undermask & overmask;
      chans[0] &= undermask & overmask;
      chans[1] &= undermask & overmask;      
      merge(chans,img);
   }
   
   void showHistogram(Mat img,vector<int> &output) {
    
     //Hold the histogram
     MatND histH;
     MatND histS;
     
     Mat histImg;
     int nbins = 45; // lets hold 256 levels
     int hsize[] = { nbins }; // just one dimension
     float range[] = { 0, 180 };
     const float *ranges[] = { range };
     int chnls[] = {0};
    
     // create colors channels
     vector<Mat> colors;
     split(img, colors);
    
     // compute for all colors
     calcHist(&colors[0], 1, chnls, colors[2], histH,1,hsize,ranges);
     //calcHist(&colors[1], 1, chnls, colors[2], histS,1,hsize,ranges);
     
     double maxHVal=0;
     //double maxSVal=0;
     minMaxLoc(histH,0,&maxHVal,0,0);
     //std::cout<< maxHVal << "," << histH << '\n';
     //minMaxLoc(histS,0,&maxSVal,0,0);
     output.clear();
     for(int i=0;i<histH.rows;i++){
        int histHValue = (int)histH.at<float>(i,0);
        //float histSValue = histS.at<float>(i,0);
        if(histHValue > maxHVal/2){
            //std::cout<< histHValue << "\n";
            output.push_back((i+1)*4-2);
        }
     }
    
   }

}



ROIExtractor::ROIExtractor(Mat sample){
   sample.copyTo(_temp);
   HSVwLim(_temp);
   showHistogram(_temp,colors);
   //std::cout<<colors.size()<<"\n";
   imshow("org",sample);
   imshow("HSV",_temp);
}

void ROIExtractor::extract(Mat frame,vector<Rect> &ROIs){
   Mat processed,conImg;
   frame.copyTo(processed);
   HSVwLim(processed);
   vector<Mat> chans;
   vector< vector<Point> > contours;
   Scalar col = sum(_temp)/(sum(_temp)[2]/255);
   split(processed,chans);
   processed = Mat::zeros(processed.size(),CV_8U);
   for(int i=0;i<colors.size();i++){
      absdiff(chans[0], Mat::ones(processed.size(),CV_8U)*colors[i],chans[1]);
      //absdiff(chans[1], Mat::ones(processed.size(),CV_8U)*col[1],chans[1]);
      threshold(chans[1],chans[1],5,255,CV_THRESH_BINARY_INV);
      //threshold(chans[1],chans[1],5,255,CV_THRESH_BINARY_INV);
   
      processed |= chans[1];
   }
   medianBlur(processed,processed,9);
   processed.copyTo(conImg);
   findContours(conImg,contours,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE);
   //drawContours(frame,contours,-1,Scalar(0,0,255),3);
   
   int min_x, min_y, max_x, max_y;
   
   for (int i=0; i< contours.size(); i++) {
      min_x = max_x = contours[i][0].x;
      min_y = max_y = contours[i][0].y;
      for(int j=0;j<contours[i].size();j++){
         min_x = min_x > contours[i][j].x? contours[i][j].x:min_x;
         max_x = max_x < contours[i][j].x? contours[i][j].x:max_x;
         min_y = min_y > contours[i][j].y? contours[i][j].y:min_y;
         max_y = max_y < contours[i][j].y? contours[i][j].y:max_y;
      }
      if(max_y-min_y > 10 && max_x-min_x> 10 )
         ROIs.push_back(Rect(min_x,min_y,max_x-min_x,max_y-min_y));
   }
   
   
   resize(processed,processed,Size(800,600));
   imshow("image",processed);
   //ROIs.push_back(Rect(480,620,100,150));
}
