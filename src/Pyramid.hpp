#ifndef PY_H
#define PY_H

#include <opencv2/opencv.hpp>
#include <vector>

using namespace cv;
using namespace std;

class Pyramid{
public:
   //all the functions
   Pyramid(Mat image, Mat data, vector< KeyPoint > kps, float ratio, vector< KeyPoint > * kp);
   Mat _image;
   Mat _data;
   vector< KeyPoint > _keypoints;
   int  _kpIndex; // offset of these keypoints in TemplateImage._allKeypoints
   float _ratio;
};
#endif
