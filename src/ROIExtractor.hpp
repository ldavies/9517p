#ifndef ROIEXTRACTOR_H
#define ROIEXTRACTOR_H
//extract roi based on HSV blobs

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <stdio.h>

using namespace cv;

class ROIExtractor{
public:
   //all the functions
   ROIExtractor(Mat templ);
   void extract(Mat frame,vector<Rect> &ROIs); 
private:
   Mat _temp;
   vector<int> colors;
};
#endif
