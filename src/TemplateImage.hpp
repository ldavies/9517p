#ifndef TI_H
#define TI_H

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <stdio.h>
#include "ROIExtractor.hpp"
#include "Pyramid.hpp"
#include <math.h>

class featureMatcher;

using namespace cv;
using namespace std;

class TemplateImage{
public:
   TemplateImage(Mat& t, featureMatcher* m);
   ~TemplateImage();

   ROIExtractor *    _roiExtractor;
   Pyramid *         _original;
   vector< Pyramid > _pyramids;
   vector< KeyPoint > _allKeypoints;
private:
   featureMatcher *  _matcher;
};

#include "featureMatcher.hpp"
#endif
