#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <iterator>

#define PI 3.14159265

using namespace std;
using namespace cv;

int pos_sOf= 86;
int pos_wl = 5;
int pos_ag = 180;
Mat src_f;
Mat dest;
Mat dest2;

const char * outfilename;

Mat mkFilter(double wl, double sOf, double ag)
{
   double tS = 0.707;
   Mat filter = Mat::ones(src_f.size(),CV_32F);
   
   Mat ang = Mat::ones(src_f.size(),CV_32F);
   Mat rad = Mat::ones(src_f.size(),CV_32F);
   Mat lp = Mat::ones(src_f.size(),CV_32F);
   double fo = 1./wl;
   for (int i = 0; i<filter.size().height; i++) {
      for(int j = 0; j<filter.size().width;j++){
         double x = ((double)(i)/(double)filter.size().height)-0.5;
         double y = ((double)(j)/(double)filter.size().width)-0.5;
         rad.at<float>(i,j) = sqrt(x*x+y*y) == 0?0:
         (float)exp((-(pow(log(sqrt(x*x+y*y)/fo),2)))/pow(2*log(sOf),2));
         lp.at<float>(i,j) = 1./(1.+ pow(sqrt(x*x+y*y)/0.45,2*15));
         double theta = atan2(-y,x);
         double sint = sin(theta);
         double cost = cos(theta);
         double ds = sint*cos(ag) - cost*sin(ag);
         double dc = cost*cos(ag) + sint*sin(ag);
         double dt = abs(atan2(ds,dc));
         ang.at<float>(i,j) = exp(-(dt*dt))/(2*tS*tS);
      }
   }
   filter = lp.mul(ang.mul(rad));
   //imshow("rad",rad);
   //imshow("ang",ang);
   //imshow("lp",lp);
   //imshow("filter",filter);
   
   
   int cx = filter.cols/2;
   int cy = filter.rows/2;
   
   //fix quadrants
   Mat q0(filter, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
   Mat q1(filter, Rect(cx, 0, cx, cy));  // Top-Right
   Mat q2(filter, Rect(0, cy, cx, cy));  // Bottom-Left
   Mat q3(filter, Rect(cx, cy, cx, cy)); // Bottom-Right
   
   Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
   q0.copyTo(tmp);
   q3.copyTo(q0);
   tmp.copyTo(q3);
   
   q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
   q2.copyTo(q1);
   tmp.copyTo(q2);
   
   
   return filter;
}

Mat gabor(Mat input,double wl, double sOf, double ag)
{
   
   Mat rlt;
   Mat complexI;
   
   std::cout<<sOf<<","<<wl<<","<<ag<<"\n";
   
   Mat I;
   cvtColor(input, I, CV_BGR2GRAY);
   Mat padded;
   int m = getOptimalDFTSize( I.rows );
   int n = getOptimalDFTSize( I.cols );
   copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_CONSTANT, Scalar::all(0));
   
   Mat planes[] = {Mat_<float>(I), Mat::zeros(I.size(), CV_32F)};
   merge(planes, 2, complexI);
   dft(complexI, complexI);
   
   Mat filter = mkFilter(wl,sOf,ag);
   Mat complexO;
   Mat img;
   split(complexI, planes);
   planes[0] = planes[0].mul(filter);
   planes[1] = planes[1].mul(filter);
   merge(planes, 2, complexO);
   
   dft(complexO, complexO,DFT_INVERSE);
   split(complexO, planes);
   normalize(planes[0], planes[0], 0, 1, CV_MINMAX);
   normalize(planes[1], planes[1], 0, 1, CV_MINMAX);
   rlt = (abs(planes[1]) - abs(planes[0]));
   threshold(rlt,rlt,0.1,1,THRESH_BINARY);
   rlt.convertTo(img,CV_8U,255,0);
   cvtColor(img,rlt,CV_GRAY2BGR);
   
   return img;
}

void Process(int , void *)
{
   vector<Vec4i> lines;
   vector<Vec4i> linesH;
   Mat rlt = gabor(src_f,5,0.86,CV_PI);
   Mat rltH = gabor(src_f,7,0.88,CV_PI/2);
   
   HoughLinesP(rlt,lines,1,CV_PI/180,50,30,10);
   Mat fre = Mat::zeros(rlt.size(),CV_8U);
   double maxlineweight = (lines.size() > 0) ? 255/(float)(lines.size()) * 10 : 0;
   cout << "Number of hough lines for vanishing point : " << lines.size() << endl;
   for(size_t i=0;i<lines.size();i++){
      double dx = lines[i][0] - lines[i][2];
      double dy = lines[i][1] - lines[i][3];
      double angle = abs(fmod(atan2(dy,dx) / (PI/2),1));
      if(angle>0.05 && angle < 0.95){
      //if(abs(lines[i][1] - lines[i][3])>5){
         double weight = maxlineweight * 2*(fmod((angle+0.5),1)-0.5);
         Mat opt = Mat::zeros(rlt.size(),CV_8U);
         line(opt,Point(lines[i][0],lines[i][1]) - Point(dx,dy)*34,Point(lines[i][2],lines[i][3]) + Point(dx,dy)*34,Scalar(weight,weight,weight),3,CV_AA);
         fre = fre + opt;
      }
   }
   
   HoughLinesP(rltH,linesH,1,CV_PI/180,50,30,10);
   src_f.copyTo(rltH);
   for(size_t i=0;i<linesH.size();i++){
      if(abs(linesH[i][0] - linesH[i][2]) < 20){
         Mat opt = Mat::zeros(rltH.size(),CV_8U);
         double dx = linesH[i][0] - linesH[i][2];
         double dy = linesH[i][1] - linesH[i][3];
         line(rltH,Point(linesH[i][0],linesH[i][1]) - Point(dx,dy)*34,Point(linesH[i][2],linesH[i][3]) + Point(dx,dy)*34,Scalar(5,255,5),1,0);
      }
   }
   
   resize(rltH,rltH,Size(800,400));
   imshow("SrcR", rltH);
   
   //imshow("SrcI", planes[1]);
   
   Point max;
   
   minMaxLoc(fre,0,0,0,&max);
   
   src_f.copyTo(rlt);
   double x,y,u,v,r,t;
   x = max.x;
   y = max.y;
   
   for(size_t i=0;i<lines.size();i++){
      u = lines[i][0];
      v = lines[i][1];
      r = lines[i][2];
      t = lines[i][3];
      u = u - r;
      v = v - t;
      r = x - r;
      t = y - t;
      double s = (u*r + v*t)/(u*u + v*v);
      u = r-u*s;
      v = t-v*s;
      if(abs(atan2(max.x-lines[i][0],max.y-lines[i][1]) - atan2(max.x-lines[i][2],max.y-lines[i][3])) < CV_PI/180.0*2){
         Mat opt = Mat::zeros(rlt.size(),CV_8U);
         double dx = lines[i][0] - lines[i][2];
         double dy = lines[i][1] - lines[i][3];
         line(rlt,max,max + 54*(Point(lines[i][2],lines[i][3])-max),Scalar(0,255,0),2,0);
         fre = fre + opt;
      }
   }
   
   
   double k = 3;
   double s = 7.;
   double w = 95;
   double h = 120;
   double ho = 13;
   
   
   vector<Point2f> src;
   vector<Point2f> des;
   vector<Point2f> wal;
   vector<Point2f> que;
   
   src.push_back(Point(max.x-(s*w/2),max.y+(s*h/2-s*ho)));
   src.push_back(Point(max.x+(s*w/2),max.y+(s*h/2-s*ho)));
   src.push_back(Point(max.x-(s*w/2),max.y-(s*h/2+s*ho)));
   src.push_back(Point(max.x+(s*w/2),max.y-(s*h/2+s*ho)));
   
   des.push_back(Point(max.x-(k*w/2),max.y+(k*h/2-k*ho)));
   des.push_back(Point(max.x+(k*w/2),max.y+(k*h/2-k*ho)));
   des.push_back(Point(max.x-(k*w/2),max.y-(k*h/2+k*ho)));
   des.push_back(Point(max.x+(k*w/2),max.y-(k*h/2+k*ho)));
   
   wal.push_back(Point(1000,0));
   wal.push_back(Point(300,0));
   wal.push_back(Point(300,500));
   wal.push_back(Point(1000,500));
   
   que.push_back(des[2]);
   que.push_back(src[2]);
   que.push_back(src[0]);
   que.push_back(des[0]);
   
   rectangle(rlt,src[0],src[3],Scalar(0,0,255),2,8,0);
   rectangle(rlt,des[0],des[3],Scalar(0,0,255),2,8,0);
   
   line(rlt,src[0],des[0],Scalar(0,0,255),2,0);
   
   line(rlt,src[1],des[1],Scalar(0,0,255),2,0);
   
   line(rlt,src[2],des[2],Scalar(0,0,255),2,0);
   
   line(rlt,src[3],des[3],Scalar(0,0,255),2,0);
   
   circle(rlt,max,5,Scalar(255,0,0),1,0);
   
   Mat H = getPerspectiveTransform(que,wal);
   Mat wall;
   warpPerspective(src_f, wall, H,Size(1000,500));
   
   resize(rlt,rlt,Size(800,400));
   resize(wall,wall,Size(800,400));
   resize(fre,fre,Size(800,400));
   imwrite(outfilename,rlt);
   imshow("Src2",wall);
   imshow("Src3",fre);
   
}

int main(int argc, char** argv)
{
   src_f = imread(argv[1],1);

   outfilename = argv[3];
   
   namedWindow("Src");
   /*
    createTrackbar("sOf", "Src", &pos_sOf, 100, Process);
    createTrackbar("wl", "Src", &pos_wl, 100, Process);
    createTrackbar("ag", "Src", &pos_ag, 360, Process);*/
   Process(0,0);

   return 0;
}
