#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <iterator>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
   //load images
   Mat left = imread(argv[1],1);
   Mat right = imread(argv[2],1);
   resize(left,left,Size(0,0),0.5,0.5);
   resize(right,right,Size(0,0),0.5,0.5);
   
   
   Mat mask;
   Mat shifted;
   
   //Affine matrix
   Mat M = Mat::zeros(Size(3,2),CV_32F);
   M.at<float>(0,0) = 1;
   M.at<float>(1,1) = 1;
   
   M.at<float>(0,2) = 0;
   M.at<float>(1,2) = -2;
   
   //create edge mask
   medianBlur(left,mask,7);
   cvtColor( mask, mask, CV_RGB2GRAY );
   Laplacian( mask, mask, -1, 5, 1, 0, BORDER_DEFAULT );
   threshold(mask,mask,128,1,THRESH_BINARY);
   
   
   Mat disp = Mat::zeros(left.size(),CV_8U);
   Mat minA = Mat::ones(left.size(),CV_8U)*255;
   
   Mat diff;
   for(int i=0;i<20;i++){
      
      //shift image
      M.at<float>(0,2) = -i+10;
      M.at<float>(1,2) = -4;
      warpAffine( right, shifted, M, right.size());
      
      //diffence in image based on gaussian distribution around a point
      
      absdiff(left,shifted,diff);
      GaussianBlur(diff,diff,Size(9,3),0,0,BORDER_DEFAULT);
      cvtColor( diff,diff, CV_RGB2GRAY );
      
      //mask out good bits
      //Mat masked = mask.mul(diff);
      
      //find the minimum over displacment
      Mat minC = min(minA,diff);
      
      //mask out the changed bits
      Mat newMin;
      absdiff(minA,minC,newMin);
      threshold(newMin,newMin,1,10,THRESH_BINARY);
      newMin = newMin * i;
      disp = max(disp,newMin);
      minA = minC;
      
   }
   
   //imshow("dif",diff);
   //resize(disp,disp,Size(0,0),0.5,0.5);
   //imshow("Left",disp);
   //waitKey(0);
   imwrite(argv[3],disp);
   return 0;
}
