#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>

#include "opencv2/video/tracking.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#define MAXWIDTH 150.0

#define __SFM__DEBUG__

using namespace cv;
using namespace std;

void getStereoFrames(Mat& in, Mat& left, Mat& right) {
  int cols = in.cols/2;
  //int outrows = in.rows/2;
  int outrows = in.rows;
  int croprows = in.rows;
  resize(in(Rect(cols,0,cols,croprows)),right,Size(cols,outrows));
  resize(in(Rect(0,0,cols,croprows)),left,Size(cols,outrows));
}

int other(int cf) { return 1-cf; }

Point matchImages(const Mat& img, const Mat& _b)
{
    //const Mat *img = &a, *_b = &b;
    Rect bounds(_b.cols/4,_b.rows/4,3*_b.cols/4,3*_b.rows/4);
    Mat templ = _b(bounds); 

    /// Create the result matrix
    int result_cols =  img.cols - templ.cols + 1;
    int result_rows = img.rows - templ.rows + 1;

    Mat result( result_cols, result_rows, CV_32FC1 );

    /// Do the Matching and Normalize
    //matchTemplate(img, templ, result, CV_TM_CCOEFF);
    //normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());
    matchTemplate(img, templ, result, CV_TM_SQDIFF);
    normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());

    Point maxLoc;
    //minMaxLoc(result, NULL, NULL, NULL, &maxLoc);
    minMaxLoc(result, NULL, NULL, &maxLoc, NULL);

    return Point(maxLoc.x - _b.cols/4, maxLoc.y - _b.rows/4);
}

int main(int argc, const char* argv[])
{
    if (argc < 3)
    {
        cerr << "Usage : " << argv[0] << "<invideo> <outvideo>" << endl;
        return -1;
    }

    VideoCapture capture(argv[1]);
        if (!capture.isOpened())
          {
            cerr << "Failed to open " << argv[1] << endl;
	    exit(0);
          }

    //capture.set(CV_CAP_PROP_POS_FRAMES,1200);

    int curframe = 0;
    int frames = 0;
    Mat frame[2];

    VideoWriter outputVideo(argv[2],CV_FOURCC('F','M','P','4'),capture.get(CV_CAP_PROP_FPS),Size(capture.get(CV_CAP_PROP_FRAME_WIDTH)/2,capture.get(CV_CAP_PROP_FRAME_HEIGHT)/2));

    while (true) {
      frames++;

      Mat bullshit;
      capture >> bullshit;
      bullshit.copyTo(frame[curframe]);

      if (frame[curframe].empty()) {
        cout << "Video finished" << endl;
        break;
      }


      if (frames<=1) {
        //outputVideo << frame[curframe];
        curframe = other(curframe);
	
	// write out frame
        continue;
      }

      Mat leftframe[2];
      Mat rightframe[2];
      getStereoFrames(frame[other(curframe)],leftframe[0],rightframe[0]);
      getStereoFrames(frame[curframe],leftframe[1],rightframe[1]);

      // adjust position of this current right frame relative to previous frame.
      Point offset = matchImages(rightframe[0],rightframe[1]);
      cout << "Curframe " << curframe << endl;
      cout << "Offset " << offset << endl;

      //outputVideo << frame[curframe];
      imshow("out", frame[curframe]);

      Mat absdiffmat;
      //absdiff(rightframe[0],rightframe[1],absdiffmat);

      absdiff(frame[curframe],frame[other(curframe)],absdiffmat);
      //normalize(absdiffmat,absdiffmat);

      //imshow("right", absdiffmat);
      //imshow("first", frame[other(curframe)]);

      curframe = other(curframe);
      //waitKey(5);

      outputVideo << leftframe[1];
    }
    waitKey();


    return 0;
}

/*
    int buildOpticalFlowPyramid(InputArray img, 
                                OutputArrayOfArrays pyramid, 
				Size winSize, 
				int maxLevel, 
				bool withDerivatives=true, 
				int pyrBorder=BORDER_REFLECT_101, 
				int derivBorder=BORDER_CONSTANT, 
				bool tryReuseInputImage=true);
     double calcGlobalOrientation(InputArray orientation, 
                                  InputArray mask, 
				  InputArray mhi, 
				  double timestamp, 
				  double duration);

*/
