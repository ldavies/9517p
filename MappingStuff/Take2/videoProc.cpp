#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/stitching/stitcher.hpp"
#include <math.h>
#include <vector>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include <fstream>

using namespace std;
using namespace cv;

Mat VPEst(Mat left,Mat right,Point* VP){
   
   Mat img;
   
   //edge extraction
   blur(left,img,Size(3,3));
   cvtColor( img, img, CV_RGB2GRAY );
   Canny( img, img, 30,70, 3 );
   
   //cvtColor( img,img, CV_GRAY2BGR );
   //return img;
   
   
#if 0
   //line fitting
   vector<Vec4i> lines;
   HoughLinesP(img,lines,1,CV_PI/180,10,15,2);
   //Generate a histogram for VP voting
   Mat fre = Mat::zeros(img.size(),CV_8U);
   for(size_t i=0;i<lines.size();i++){
      //filter out horizontal and vertical line
      if(abs(lines[i][0] - lines[i][2])>5 && abs(lines[i][1] - lines[i][3])>5){
         Mat opt = Mat::zeros(img.size(),CV_8U);
         double dx = lines[i][0] - lines[i][2];
         double dy = lines[i][1] - lines[i][3];
         line(opt,Point(lines[i][0],lines[i][1]) - Point(dx,dy)*34,Point(lines[i][2],lines[i][3]) + Point(dx,dy)*34,Scalar(25,25,25),1,0);
         GaussianBlur(opt,opt,Size(5,5),0);
         fre = fre + opt;
      }
   }
#elif 0
   
   //other method
   vector<Vec2f> lines;
   HoughLines( img, lines, 1, CV_PI/180, 70 );
   //Generate a histogram for VP voting
   Mat fre = Mat::zeros(img.size(),CV_8U);
   for( size_t i = 0; i < lines.size(); i++ )
   {
      float rho = lines[i][0];
      float theta = lines[i][1];
      double a = cos(theta), b = sin(theta);
      if(abs(a) > 0.2 && abs(b) > 0.2){
         double x0 = a*rho, y0 = b*rho;
         Mat opt = Mat::zeros(img.size(),CV_8U);
         Point pt1(cvRound(x0 + 1000*(-b)),
                   cvRound(y0 + 1000*(a)));
         Point pt2(cvRound(x0 - 1000*(-b)),
                   cvRound(y0 - 1000*(a)));
         line( opt, pt1, pt2, Scalar(25,25,25),1, 8 );
         GaussianBlur(opt,opt,Size(5,5),0);
         fre = fre + opt;
      }
   }
   
#else
   
   //VPLF
   Mat fre = Mat::zeros(img.size(),CV_32F);
   vector<Point> edgePoint;
   
   //extract all edge pixels and store
   int wid = img.size().width;
   int hei = img.size().height;
   for(int i=0;i<wid;i++){
      for(int j=0;j<hei;j++){
         if(img.at<uchar>(j,i)>128){
            edgePoint.push_back(Point(i,j));
         }
      }
   }
   
   Mat mask = Mat::zeros(img.size()*2,CV_32F);
   int mwid = mask.size().width/2;
   int mhei = mask.size().height/2;
   for(int i=0;i<mask.size().width;i++){
      for(int j=0;j<mask.size().height;j++){
         mask.at<float>(j,i) =0.5- atan2(mhei-j,mwid-i)/(CV_PI*2);
      }
   }
   
   Mat edge;
   img.convertTo(edge,CV_32F,1./255.);
   
   int res = 360; // resolution of the VP histogram
   int stp = 3;

   
   int chnls[] = {0};
   int hsize[] = { res }; // just one dimension
   float range[] = { 0, 1 };
   const float *ranges[] = { range };
   
   //for each potential VP
   for (int i= 100; i<wid-140; i+=stp) {
      for (int j=100; j<140; j+=stp) {
         Mat roimask(mask,Rect(wid-i,hei-j,wid,hei));
         Mat mixed = edge.mul(roimask);
         MatND hist;
         calcHist(&mixed,1, chnls, Mat(), hist, 1, hsize,ranges);

         /*
          //compute squared distribution for that VP
          vector<int> hist;
          for (int k=0; k<res; k++) {
          hist.push_back(0);
          }
          //precalculate area cache
          double the1 = atan2(-j,-i);
          double the2 = atan2(-j,wid-i);
          double the3 = atan2(hei-j,wid-i);
          double the4 = atan2(hei-j,-i);
          double are1 = i*j;
          double are2 = wid*j;
          double are3 = are1 + hei*(wid-i);
          double are4 = wid*hei;
          //tally the histogram for all edge points
          for(int k=0;k<edgePoint.size();k++){
          //find area
          double the = atan2(edgePoint[k].y-j,edgePoint[k].x-i);
          double area = 0;
          if(the < the1){
          double yint = tan(the)*i;
          area = 0.5 * yint * i;
          }else if(the < the2){
          double xint = tan(CV_PI/2. - the)*j;
          area = are1 - 0.5*xint*j;
          }else if(the < the3){
          double yint = tan(CV_PI - the)*(wid-i);
          area = are2 - 0.5*yint*(wid-i);
          }else if(the < the4){
          double xint = tan(CV_PI*1.5-the)*(hei-j);
          area = are3 - 0.5*xint*(hei-j);
          }else{
          double yint = tan(CV_PI*2-the)*i;
          area = are4 - 0.5*yint*i;
          }
          
          //normalise by dividing by wid*hei
          double fv = area/(hei*wid);
          //add to histo
          int index = (int)(fv*(res-1));
          if(index < 0 || index >= res)std::cout<<the<<"\n";
          hist[index] ++;
          }
          //square and sum the distribution
          int vpld = 0;
          for (int k=0; k<res; k++) {
          vpld += hist[k]*hist[k];
          }
          for(int r=0;r<stp;r++){
          for(int s=0;s<stp;s++){
          fre.at<float>(j+s,i+r) = (float)vpld;
          }
          }
          */
         hist.at<float>(0) = 0;
         double vpld = 0;
         double dx = hist.at<float>(1);
         for(int r=3;r<res;r++){
            vpld += abs(hist.at<float>(r)-hist.at<float>(r-2))*abs(hist.at<float>(r)-hist.at<float>(r-2));
         }
         for(int r=0;r<stp;r++){
            for(int s=0;s<stp;s++){
               fre.at<float>(j+s,i+r) = (float)vpld;
            }
         }
      }
   }
   normalize(fre,fre,0,1,NORM_MINMAX, -1, Mat());
   fre.convertTo(fre,CV_8U,255);
   
#endif
   
   
   //extract the point with most votes
   Point max;
   double cnt;
   minMaxLoc(fre,0,&cnt,0,&max);
   fre += img;
   //Draw pretty picture to show what is happening
   cvtColor( fre,img, CV_GRAY2BGR );
   line(img,Point(0,120),Point(left.size().width,120),Scalar(255,0,0),1,0);
   //if(cnt > 10 && abs(max.y -120)<10){
   //mean over n time of VP
   (*VP).x = ((*VP).x * 5 + max.x)/6;
   (*VP).y = ((*VP).y * 5 + max.y)/6;
   circle(img,*VP,5,Scalar(0,255,0),1,0);
   // }
   
   return img;
}

Point rotate(Point p,Point c,double ang){
   p = p - c;
   Point final = Point(p.x * cos(ang)-p.y*sin(ang),p.y * cos(ang)+p.x*sin(ang));
   final = final + c;
   return final;
}

Mat reProj(Mat left, Mat right,Mat &lWall,Mat &rWall,Point VP){
   
   Mat img=Mat::zeros(left.size(),CV_8UC3);
   
   //over lay template
   double k = .4; //further scale
   double s = 1.4; //closer scale
   double w = 260; //corridor width
   double h = 340; //height
   double ho = 90; // camera height offset
   double wo = -00; //camera width offset
   double hs = 5;
   double ws = 10;
   
   VP.x += ws;
   VP.y += hs;
   
   vector<Point2f> src;
   vector<Point2f> des;
   vector<Point2f> wal;
   vector<Point2f> lque;
   vector<Point2f> rque;
   double angle = -0.01;
   
   src.push_back(rotate(Point(VP.x-(s*w/2+s*wo),VP.y+(s*h/2-s*ho)),VP,angle));
   src.push_back(rotate(Point(VP.x+(s*w/2-s*wo),VP.y+(s*h/2-s*ho)),VP,angle));
   src.push_back(rotate(Point(VP.x-(s*w/2+s*wo),VP.y-(s*h/2+s*ho)),VP,angle));
   src.push_back(rotate(Point(VP.x+(s*w/2-s*wo),VP.y-(s*h/2+s*ho)),VP,angle));
   
   des.push_back(rotate(Point(VP.x-(k*w/2+k*wo),VP.y+(k*h/2-k*ho)),VP,angle));
   des.push_back(rotate(Point(VP.x+(k*w/2-k*wo),VP.y+(k*h/2-k*ho)),VP,angle));
   des.push_back(rotate(Point(VP.x-(k*w/2+k*wo),VP.y-(k*h/2+k*ho)),VP,angle));
   des.push_back(rotate(Point(VP.x+(k*w/2-k*wo),VP.y-(k*h/2+k*ho)),VP,angle));
   
   
   //orthogonal plane to reproject to
   wal.push_back(Point(left.size().width,0));
   wal.push_back(Point(0,0));
   wal.push_back(Point(0,left.size().height));
   wal.push_back(Point(left.size().width,left.size().height));
   
   //plane to project from left
   lque.push_back(des[2]);
   lque.push_back(src[2]);
   lque.push_back(src[0]);
   lque.push_back(des[0]);
   
   //plane to project from right
   rque.push_back(des[3]);
   rque.push_back(src[3]);
   rque.push_back(src[1]);
   rque.push_back(des[1]);
   
   circle(img,lque[0],3,Scalar(0,255,0),1,0);
   circle(img,lque[1],3,Scalar(255,0,0),1,0);
   circle(img,lque[2],3,Scalar(0,0,255),1,0);
   circle(img,lque[3],3,Scalar(255,255,0),1,0);
   
   line(img,src[0],src[1],Scalar(0,0,255),1,0);
   line(img,src[0],src[2],Scalar(0,0,255),1,0);
   line(img,src[2],src[3],Scalar(0,0,255),1,0);
   line(img,src[3],src[1],Scalar(0,0,255),1,0);
   
   line(img,des[0],des[1],Scalar(0,0,255),1,0);
   line(img,des[2],des[0],Scalar(0,0,255),1,0);
   line(img,des[2],des[3],Scalar(0,0,255),1,0);
   line(img,des[3],des[1],Scalar(0,0,255),1,0);
   
   line(img,src[0],des[0],Scalar(0,0,255),1,0);
   line(img,src[1],des[1],Scalar(0,0,255),1,0);
   line(img,src[2],des[2],Scalar(0,0,255),1,0);
   line(img,src[3],des[3],Scalar(0,0,255),1,0);
   
   Mat Hl = getPerspectiveTransform(lque,wal);
   warpPerspective(left,lWall,Hl,left.size());
   
   Mat Hr = getPerspectiveTransform(rque,wal);
   warpPerspective(left,rWall,Hr,left.size());
   
   return img;
}

Point matchImages(const Mat img, const Mat _b)
{
    //const Mat *img = &a, *_b = &b;
    Rect bounds(_b.cols/4,_b.rows/4,3*_b.cols/4,3*_b.rows/4);
    Mat templ = _b(bounds); 

    /// Create the result matrix
    int result_cols =  img.cols - templ.cols + 1;
    int result_rows = img.rows - templ.rows + 1;

    Mat result( result_cols, result_rows, CV_32FC1 );

    /// Do the Matching and Normalize
    matchTemplate(img, templ, result, CV_TM_CCOEFF);
    normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());
    //matchTemplate(img, templ, result, CV_TM_SQDIFF);
    //normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());

    Point maxLoc;
    minMaxLoc(result, NULL, NULL, NULL, &maxLoc);
    //minMaxLoc(result, NULL, NULL, &maxLoc, NULL);

    return Point(maxLoc.x - _b.cols/4, maxLoc.y - _b.rows/4);
}

int main(int argc, char** argv)
{
   VideoCapture cap(argv[1]);
   if(!cap.isOpened())return -1;
   
   VideoWriter outputVideo(argv[2],CV_FOURCC('J','P','E','G'),25,Size(cap.get(CV_CAP_PROP_FRAME_WIDTH)/2,cap.get(CV_CAP_PROP_FRAME_HEIGHT)/2));
   Mat curImg;
   Mat left;
   Mat right;
   Mat final = Mat::zeros(Size(cap.get(CV_CAP_PROP_FRAME_WIDTH)/2,cap.get(CV_CAP_PROP_FRAME_HEIGHT)/2),CV_8UC3);
   Mat lWall = Mat(final,Rect(0,0,cap.get(CV_CAP_PROP_FRAME_WIDTH)/4,cap.get(CV_CAP_PROP_FRAME_HEIGHT)/4));
   Mat rWall = Mat(final,Rect(cap.get(CV_CAP_PROP_FRAME_WIDTH)/4,0,cap.get(CV_CAP_PROP_FRAME_WIDTH)/4,cap.get(CV_CAP_PROP_FRAME_HEIGHT)/4));
   Mat disp= Mat(final,Rect(0,cap.get(CV_CAP_PROP_FRAME_HEIGHT)/4,cap.get(CV_CAP_PROP_FRAME_WIDTH)/4,cap.get(CV_CAP_PROP_FRAME_HEIGHT)/4));
   Mat org= Mat(final,Rect(cap.get(CV_CAP_PROP_FRAME_WIDTH)/4,cap.get(CV_CAP_PROP_FRAME_HEIGHT)/4,cap.get(CV_CAP_PROP_FRAME_WIDTH)/4,cap.get(CV_CAP_PROP_FRAME_HEIGHT)/4));
   Point VP(130,120);
   cap.set(CV_CAP_PROP_POS_FRAMES,1200);
   
   Mat oldWall;
   Mat fullWall;
   for(int i=0;i<1000;i++){
      cap.set(CV_CAP_PROP_POS_FRAMES,1200+i*10);
      cap >> curImg;
      if(curImg.empty())break;
      Mat output;
      left = Mat(curImg,Rect(0,0,curImg.size().width/2,curImg.size().height));
      right = Mat(curImg,Rect(curImg.size().width/2,0,curImg.size().width/2,curImg.size().height));
      resize(left,left,Size(0,0),.5,.25,INTER_NEAREST);
      resize(right,right,Size(0,0),.5,.25,INTER_NEAREST);
      output = VPEst(left,right,&VP);
      output += reProj(left,right,lWall,rWall,VP);
      //output += left;
      left.copyTo(org);
      output.copyTo(disp);
      outputVideo << final;
      if(i!=0){
         Point off = matchImages(oldWall,lWall);
         std::cout<<off<<"\n";
         Mat newWall = Mat::zeros(Size(fullWall.size().width-off.x,fullWall.size().height),CV_8UC3);
        
         Mat roiNew(newWall,Rect(0,0,lWall.size().width,lWall.size().height));
         lWall.copyTo(roiNew,lWall);
         
         Mat roiWal(newWall,Rect(-off.x,0,fullWall.size().width,fullWall.size().height));
         fullWall.copyTo(roiWal,fullWall);
         
         imwrite("WALLLLLLLLLLLBITCHES.jpg",newWall);
         newWall.copyTo(fullWall);
      }else{
         lWall.copyTo(fullWall);
         
      }
      lWall.copyTo(oldWall);
   }
   //imshow("WALL!!!!!!!!!!!",LStitch);

   return 0;
}
